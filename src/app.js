const express = require("express");
const fs = require("fs-promise");
const ejs = require('ejs');
let config = require('./config.js');


const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const crypto = require('crypto');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

let storage = require('./modules/storage');

const app = express();

let mongoose = require('mongoose');


app.set("view engine", "ejs");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(busboyBodyParser()); 
app.use(express.static("public"));

// io = require('socket.io').listen(app);


var http = require('http');

var server = http.createServer(app);
var io = require('socket.io').listen(server);
io.set('log level', 1);
io.set('resource', '/api');



mongoose.connect('mongodb://kyrsa4:kyrsa41234@ds163595.mlab.com:63595/heroku_8s6w1t8c', {useMongoClient: true});

let cloudinary = require('cloudinary');

cloudinary.config({ 
    cloud_name: 'dbd6qr3td', 
    api_key: '694758656112573', 
    api_secret: '6Xa6cCDAeODLX6Q0osVQjvhn6so' 
  });



app.use(cookieParser());
app.use(session({
    secret: 'sdfjhsdfkdjshfgds$25_',
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function (user, done) {
    done(null, user.username);
});

passport.deserializeUser(function (name, done) {
    storage.Users.findOne({username:name})
        .then(user => {
            if (!user) done("No user");
            else done(null, user);
        });
});

passport.use(new LocalStrategy(
    function (name, plpassword, done) {
        let passHash = sha512(plpassword, serverSalt).passwordHash;
        storage.Users.findOne({username:name, passwordHash:passHash})
            .then(user => {
                if (!user) return done("No user", false);
                return done(null, user);
            });
    }
));

const serverSalt = "yuejd%#$%hdf";

function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

function checkAdmin(req, res, next) {
    if (!req.user) return res.render("err",{user: null, err:'401', errmess:"Sorry! Unauthorized"});
    if (req.user.role !== 'admin') res.render("err",{user: req.user, err:'403', errmess:"Sorry! you are not admin"});
    next();
}

function checkAuth(req, res, next) {
    if (!req.user) res.render("err",{user:null, err:'401', errmess:"Sorry! Unauthorized"});
    next();
}


app.get("/", (req, res) =>{ res.render("index",{user: req.user})});

app.get('/gameroom', (req, res)=>{
    res.render('gameroom');
})

app.get('/register',
    (req, res) => res.render('register', {
        user: req.user, 
        err:0
}));

app.post('/register',
    (req, res) => {
    let passHash = sha512(req.body.password, serverSalt).passwordHash;
    //let base64String ="data:image/png;base64," + req.files.image.data.toString('base64');
    cloudinary.uploader.upload("http://alldes.net/images/photoshop/button/id243/1.jpg", function(result) { 
        let user = new storage.Users({
            username: req.body.name,
            passwordHash: passHash,
            total: 0,
            win: 0,
            role: 'user',          
            image: `${result.url}`,
            imageid:`${result.public_id}`
        });
        user.save()
            .then(x=>res.redirect('/'))
            .catch(error=>res.render("register", {user: req.user, err:1}));
    }); 
});


app.get('/profile/:username(\\w+)',checkAuth, (req, res) => {
    let nic = req.params.username;
    console.log(nic);
    storage.Users.findOne({username: nic})
    .then(resolve => res.render("profile",{
        pl:resolve,
        user: req.user
     }));
});

app.get('/login',
    (req, res) => res.render('login', {
        user: req.user
    }));

app.post('/login',
    passport.authenticate('local'),
    (req, res) => res.redirect('/'));

app.get('/logout',
    (req, res) => {
        req.logout();
        res.redirect('/');
});

app.get('/search', (req, res)=>{
    let nic = req.query.username;
    console.log(nic);
    storage.Users.findOne({username: nic})
    .then(resolve => res.render("profile",{
        pl:resolve,
        user: req.user
     }));
});



app.post('/user/update',  checkAdmin, function(req, res){
    let note = {};
        if(req.files.image){
            cloudinary.uploader.destroy(req.user.imageid, (x) => console.log("destroy img"));
            let base64String ="data:image/png;base64," + req.files.image.data.toString('base64');
            cloudinary.uploader.upload(base64String, function(result) {
                let note = {
                    image: `${result.url}`,
                    imageid:`${result.public_id}`
                };
                storage.Users.update({username: req.user.username}, note, (err, result) => {
                    if (err) {
                        res.send({'error':'An error has occurred'});
                    } else {
                        res.redirect(`/profile/${req.user.username}`);
                    } 
                });
            });
        }
    });


app.post('/deleteuser',
(req, res) => {
    storage.Users.remove({username: req.body.username})
        .then(x => {
            cloudinary.uploader.destroy(req.body.imageid, (x) => console.log("destroy img"));
            console.log("removed");
        });
    res.redirect('/admin');
});

app.get('/admin',
checkAdmin,
(req, res) => {
    storage.Users.find()
        .then(x=>{res.render("admin",{x, user: req.user})})
});

app.get('/getStatsRoom', (req, res)=>{
    storage.Rooms.findOne({$or:[
        {pl1: req.user.username},
        {pl2: req.user.username}
    ]})
        .then(x=>{
            console.log(x);
            if(x.pl1 == req.user.username){
                res.json({you:`${x.pl1}`, op:`${x.pl2}`, roomId:`${x.roomId}`, step: x.start});
            }
            else if(x.pl2 == req.user.username){
                res.json({you:`${x.pl2}`, op:`${x.pl1}`, roomId:`${x.roomId}`, step: !x.start});
            }
        })
});

app.get("*", (req, res) =>{ res.render("err",{user: req.user, err:'404', errmess:"Sorry ! the page you are looking for can't be found"})});

app.post("/createroom", (req, res)=>{
    let room = new storage.Rooms({
        pl1: req.user.username,
        pl2: "non",
        roomId: req.body.roomId,
        start: false
    });
    room.save()
        .then(x=>{
            //console.log(x)
            res.redirect('gameroom')
        })
})

app.post("/joinroom", (req, res)=>{
    let r = {
        pl2: req.user.username,
        start: true
    }
    storage.Rooms.findOneAndUpdate({roomId: req.body.roomId, start:false}, r)
        .then(x=>{
            if(x) res.redirect('gameroom');
            else res.redirect('/');
    })   
})




io.sockets.on('connection', function (socket) {
    let room = 0;
    socket.on('room', (data)=>{
        room = data.room;
        console.log(room);
        socket.join(room);
    });
    socket.on('youstep', (data)=>{
        console.log(data)
        socket.broadcast.to(room).emit('opstep', data);
    })
    socket.on('finish', (data)=>{
        console.log(data)
        storage.Rooms.findOneAndRemove({roomId: data.room});
        storage.Users.findOne({username: request.body.username})
        .then(x => {
            x.total +=1;
            if(data.status == 'win') x.win +=1;
            x.save(); 
        });
    })
    
});







storage.Users.find()
    .then(x=> console.log(x));


server.listen(config.port, () => console.log("UP!"));