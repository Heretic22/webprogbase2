let fs = require('fs-promise');

const mongoose = require('mongoose');

//mongoose.connect('mongodb://localhost:27017/players');

mongoose.connect('mongodb://kyrsa4:kyrsa41234@ds163595.mlab.com:63595/heroku_8s6w1t8c', {useMongoClient: true});

let Users = mongoose.model('Users',{ 
    username: {type: String, unique:true},
    passwordHash: String,
    total: Number,
    win: Number,
    role: String,
    image: String,
    imageid: String
});

let Rooms = mongoose.model('Rooms', {
    pl1: String,
    pl2: String,
    roomId: {type: String, unique:true},
    start: Boolean
});

module.exports = {
    Users,
    Rooms
}