
Vue.use(VueResource);

new Vue({
  el: "body",
  data: {
    endpoint: '/api/v1/players',
    players: [],
    currentpage: 1,
    next: false,
    prev: false,
    current: {}
},
methods: {
    getAllPlayers: function() {
        let options = {
            params: {
                page: this.currentpage
            }
        }
        this.$http.get(this.endpoint, options).then(function(response) {
          this.next = false;
          if(this.currentpage > 1) this.prev = true;
          else this.prev = false;
           this.players = response.data
          if(this.players[5] && this.players[5].nextpage != 'no page'){
            console.log(this.players[5].nextpage)
            this.next = true;
          }
          this.players.pop();
          this.current = this.players[0]
        }, function(error) {
            console.log(error);
        })
    },
    getNextPage: function(){
      if(this.next){
        this.currentpage = this.currentpage + 1;
        this.getAllPlayers()
      }
    },
    getPrevPage: function(){
      if(this.prev){
        this.currentpage  = this.currentpage - 1;
        this.getAllPlayers()
      }
    },
    currentView: function(nickname){
      this.current = this.players.find(player => player.nickname == nickname);
    }
},
created: function() {
     this.getAllPlayers()
}
})