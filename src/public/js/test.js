// window.onload = function() {
// 	// Создаем соединение с сервером; websockets почему-то в Хроме не работают, используем xhr
// 	if (navigator.userAgent.toLowerCase().indexOf('chrome') != -1) {
// 		//socket = io.connect('http://localhost:8080', {'transports': ['xhr-polling']});
// 	} else {
// 		socket = io.connect(window.location.hostname + ':8080', {resource: 'api'});
// 	}
// 	socket.on('connect', function () {
// 		socket.on('msg', (msg) =>{
// 			console.log(msg);
// 		})
            
//     })
// };



//Vue.use(VueSocketio, `http://${window.location.hostname + ':8080'}`);

Vue.use(VueResource);

socket = io.connect(window.location.hostname + ':8080', {resource: 'api'});

let vue = new Vue({
	data:{
		endpoint: '/getStatsRoom',
		step:false,
		you:{
			name:"",
			money: 2000,
			curr: 1,
			myCells:[]
		},
		opon:{
			name:"",
			money: 2000,
			curr: 1,
			opCells:[]
		},
		room: 0,
		status:'',
		cellOp: false,
		cellMy:false,
		canBuy:false,
		pay:false,
		end:false,
		cells:[
			{
				type:"start"
			},
			{
				type: "cell",
				prise: 100,
				tribute: 20
			},
			{
				type:"tribute",
				value:200
			},
			{
				type: "cell",
				prise: 150,
				tribute: 30
			},
			{
				type:"money",
				value:100
			},
			{
				type: "cell",
				prise: 250,
				tribute: 200
			},
			{
				type: "cell",
				prise: 170,
				tribute: 40
			},
			{
				type:"tribute",
				value:100
			},
			{
				type: "cell",
				prise: 170,
				tribute: 40
			},
			{
				type: "cell",
				prise: 180,
				tribute: 50
			},
			{
				type: "prison",
				value:500
			},
			{
				type: "cell",
				prise: 190,
				tribute: 50
			},
			{
				type:"money",
				value:200
			},
			{
				type: "cell",
				prise: 200,
				tribute: 60
			},
			{
				type: "cell",
				prise: 200,
				tribute: 60
			},
			{
				type: "cell",
				prise: 250,
				tribute: 200
			},
			{
				type: "cell",
				prise: 210,
				tribute: 70
			},
			{
				type:"tribute",
				value:50
			},
			{
				type: "cell",
				prise: 210,
				tribute: 70
			},
			{
				type: "cell",
				prise: 220,
				tribute: 80
			},
			{
				type:"tax",
				value:400
			},
			{
				type: "cell",
				prise: 230,
				tribute: 90
			},
			{
				type:"tribute",
				value:250
			},
			{
				type: "cell",
				prise: 230,
				tribute: 90
			},
			{
				type: "cell",
				prise: 240,
				tribute: 100
			},
			{
				type: "cell",
				prise: 250,
				tribute: 200
			},
			{
				type: "cell",
				prise: 260,
				tribute: 110
			},
			{
				type: "cell",
				prise: 260,
				tribute: 110
			},
			{
				type:"money",
				value:250
			},
			{
				type: "cell",
				prise: 270,
				tribute: 120
			},
			{
				type: "prison",
				value:500
			},
			{
				type: "cell",
				prise: 280,
				tribute: 130
			},
			{
				type: "cell",
				prise: 290,
				tribute: 140
			},
			{
				type:"tribute",
				value:150
			},
			{
				type: "cell",
				prise: 300,
				tribute: 150
			},
			{
				type: "cell",
				prise: 250,
				tribute: 200
			},
			{
				type:"money",
				value:150
			},
			{
				type: "cell",
				prise: 350,
				tribute: 250
			},
			{
				type:"tribute",
				value:200
			},
			{
				type: "cell",
				prise: 400,
				tribute: 300
			}
		]
	},
	el: "body",
	methods:{
		checkCell: function(){
			this.cellOp = (this.opon.opCells.indexOf(this.you.curr-1) != -1 && this.cells[this.you.curr-1].type == "cell");
			if(this.cellOp) this.step = false;
			this.cellMy = (this.you.myCells.indexOf(this.you.curr-1) != -1);
			if(!this.cellOp && !this.cellMy && this.cells[this.you.curr-1].type == "cell"){
				if(this.you.money > 0 && this.you.money > this.cells[this.you.curr-1].prise) this.canBuy = true;
				else this.canBuy = false;
			}
			else this.canBuy = false;
			if(this.cells[this.you.curr-1].type == "money") this.you.money += this.cells[this.you.curr-1].value;
			if(this.cells[this.you.curr-1].type == "tribute") this.you.money -= this.cells[this.you.curr-1].value;
			if(this.cells[this.you.curr-1].type == "prison"){
				this.you.curr = 11;
				this.cellOp = false;
				this.pay = true;
				
			}
			if(this.cells[this.you.curr-1].type == "tax"){
				this.cellOp = false;
				this.pay = true;
				
			}
			this.step = false;
			console.log(this.cells[this.you.curr-1].type)
		},
		steep: function(){
			let s = Math.floor(Math.random() * (12 - 1 + 1)) + 1;
			this.you.curr += s;
			if(this.you.curr > 40){ 
				this.you.curr = this.you.curr - 40;
				this.you.money += 500;
			}; 
			this.checkCell();
			console.log(this.you.curr);
			console.log(this.you.money);
	
				socket.emit('youstep', this.you);
				if(this.you.money <= 0) this.finish('lose');
				
			
		},
		buyCell: function(){
			if(!this.cellMy && this.canBuy){
				this.you.myCells.push(this.you.curr-1);
				this.cellMy = true;
				this.canBuy = false;
				this.you.money -= this.cells[this.you.curr-1].prise;
				this.step = false;
				socket.emit('youstep', this.you);
				
			}
		}, 
		checkMy: function(x){
			return (this.you.myCells.indexOf(x-1) != -1);
		},
		checkOp: function(x){
			return (this.opon.opCells.indexOf(x-1) != -1);
		},
		payTribute: function(){
			if(this.cellOp){
				this.cellOp = false;
				this.step = false;
				this.you.money -= this.cells[this.you.curr-1].tribute;
			}
			if(this.pay){
				this.pay = false;
				this.step = false;
				this.you.money -= this.cells[this.you.curr-1].value;
			}
			if(this.you.money <= 0) this.finish('lose');
		},
		stats: function(){
			this.$http.get(this.endpoint).then(function(response) {
				console.log(response.data)
				this.you.name = response.data.you;
				this.opon.name = response.data.op;
				this.room = response.data.roomId;
				this.step = response.data.step;
				socket.emit('room', {room: `${this.room}`});
				
			})
			
		},
		update: function(data){
			this.opon.curr = data.curr;
			this.opon.money = data.money;
			if(this.opon.money <= 0) this.finish('win');
			this.opon.opCells = data.myCells;
			this.step = true;
		},
		finish: function(status){
			this.status = status;
			this.step = false;
			this.end = true;
			socket.emit('finish', {status: status, room: room});
		}
	},
	ready: function(){
		//this.stats();
		//console.log(this.step)
		this.stats();
		socket.on('connect', function () {
			socket.on('opstep', (data)=>{
				console.log(data);
				vue.update(data);
			})
			
		})
		
		
	}
})